# shortcut

This is a small geoguessing game I made for my father's birthday party. This is my first
experience with Rust and GUI programming. So this should rather be considered a learning
project. The game is about guessing different locations in maps showing only the boundaries
of different municipalities, regions, countries and continents. The data for the boundaries
is provided as shapefiles and retrieved from the following online sources:

- Gemeinden in Oberösterreich: https://www.doris.at/
- Bundesländer in Austria: [eurostat NUTS](https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/administrative-units-statistical-units/nuts) level 2
- Countries in Europe: [eurostat NUTS](https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/administrative-units-statistical-units/nuts) level 0
- Continents: Shepherd, Stephanie (2020): Continent Polygons. figshare. Dataset.
  https://doi.org/10.6084/m9.figshare.12555170.v3

The shapefiles are downloaded from
[here](https://littleearthling.owncube.com/index.php/s/2imMQ2MPyBfsCBa/) at the first run of
the game and stored in `$XDG_DATA_DIR/shortcut/assets`.

## Install

Make sure you have [Rust](https://www.rust-lang.org/), [Proj](https://proj.org/) and
[clang](https://clang.llvm.org/) installed.
On Arch-based linux distributions you can run the following commands.

```fish
sudo pacman -S rustup proj clang
rustup default stable
```

Afterwards, clone the shortcut repo, navigate to your local clone and install shortcut with
cargo.

```fish
git clone https://codeberg.org/daschw/shortcut.git
cd shortcut
cargo install --path=.
```

Now you can run shortcut with `~/.cargo/bin/shortcut`. If you just want to type the
`shortcut` command you have to add your `~/.cargo/bin/` directory to your `PATH` environment
variable. You can do this by adding the following line to your `~/.bashrc` file.

```sh
export PATH="$HOME/.cargo/bin:$PATH"
```

Now after restarting your terminal you should be able to run shortcut with the `shortcut`
command.

## Usage

With `shortcut` you can run the default game of two players Tux and Lua with a single map.
If you want to specify your own game you can run shortcut with
`shortcut --game=path/to/my/game.toml`.

For example, if you got an email with an attachment `christmas.toml` and you saved this
attachement in your `Downloads` folder, you could run this game with:

```sh
shortcut --game="$HOME/Downloads/christmas.toml"
```
