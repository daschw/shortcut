use crate::{player::Player, task::Task};
use clap::Parser;
use iced::Color;
use serde::Deserialize;
use std::{fs, fs::File, num::ParseIntError, path::Path};

/// The command line arguments for shortcut.
#[derive(Parser)]
#[command(
    author,
    version,
    about,
    long_about = "
shortcut is a simple geoguessing game for multiple players.

The players are presented the boundaries of a map and a question hinting to a
specific point on the map. They take turns guessing the target position.
After all players have guessed the target and the distances to all guesses of
all players are revealed.

The player furthest away from the target gets zero points.
The second-furthest guess is rewarded with one point, and so on.

Then the next task with the next map and the next question is presented.
The player with the highest total score starts guessing.
The player with the second-highest total score comes next, and so on.

After all tasks are completed the scores per task and the final scores of all
players are revealed.

The default game is configured in `$XDG_CONFIG_DIR/shortcut/config.toml`.
It defines two players Tux and Lua and a set of player colors. Furthermore, it
includes two tasks looking for the highest points in Romania and Austria.

You can define your own games in TOML files using the default config as as
a template. You don't have to set all parameters. The parameters in the default
config are used as fallback. You can pass your own config to shortcut with

    shortcut --game path/to/your/config.toml

If you don't want to define your own tasks there are predefined examples
available in `$XDG_CONFIG_DIR/shortcut/examples/`. These are game config TOML
files only containing tasks. You can invoke them with

    shortcut --example name

where `name` is the name of the example file without the path and the file
extension. Currently the following predefined examples exist:

* `seven_summits` - `$XDG_CONFIG_DIR/shortcut/examples/seven_summits.toml`

You can add your own examples by creating config TOML files in the examples
folder.

If you want to play a game or example with different players you can run

    shortcut --game mygame.toml --colors \"#FF0000\" \"#00FF00\"

or

    shortcut --game seven_summits --players Bobby Tinka"
)]
pub struct Args {
    /// Predefined example to play: seven_summits
    #[arg(short, long, value_name = "NAME")]
    pub example: Option<String>,
    /// Path to a TOML file defining a game.
    #[arg(short, long, value_name = "FILE")]
    pub game: Option<String>,
    /// Names of the players separated by spaces.
    #[arg(short, long, num_args = 1.., value_name = "NAME")]
    pub players: Option<Vec<String>>,
    /// Player colors in the form "#FF0000" separated by spaces.
    #[arg(short, long, num_args = 1.., value_name = "HEXCODE")]
    pub colors: Option<Vec<String>>,
}

#[derive(Clone, Debug, Deserialize, Default)]
pub struct Settings {
    players: Option<Vec<String>>,
    tasks: Option<Vec<Task>>,
    colors: Option<Vec<String>>,
}

impl Settings {
    pub fn new(args: &Args) -> Self {
        let xdg_dirs = xdg::BaseDirectories::with_prefix("shortcut")
            .expect("unable to find xdg base directories");
        let settings = Self::read(&xdg_dirs.find_config_file("config.toml"));
        settings
            .with_example(&args.example)
            .merge(Self::read(&args.game))
            .with_players(&args.players)
            .with_colors(&args.colors)
    }

    fn read<P: AsRef<Path>>(path: &Option<P>) -> Self {
        match path {
            Some(path) => match fs::read_to_string(path) {
                Ok(content) => toml::from_str(&content).unwrap_or(Self::default()),
                _ => Self::default(),
            },
            _ => Self::default(),
        }
    }

    pub fn tasks(&self) -> Vec<Task> {
        match &self.tasks {
            Some(tasks) => tasks.clone(),
            _ => vec![Task::default()],
        }
    }

    pub fn colors(&self) -> Vec<Color> {
        match &self.colors {
            Some(colors) => colors
                .iter()
                .map(|hex_code| Self::as_color(hex_code).unwrap())
                .collect(),
            _ => ["#FF0000", "#00FF00"]
                .iter()
                .map(|hex_code| Self::as_color(hex_code).unwrap())
                .collect(),
        }
    }

    pub fn players(&self) -> Vec<Player> {
        let tasks = self.tasks();
        let colors = self.colors();
        match &self.players {
            Some(players) => players.clone(),
            _ => vec!["Player 1".to_string(), "Player 2".to_string()],
        }
        .iter()
        .enumerate()
        .map(|(i, name)| Player::new(name, colors[i % colors.len()], tasks.len()))
        .collect::<Vec<Player>>()
    }

    fn with_players(&self, players: &Option<Vec<String>>) -> Self {
        match players {
            None => self.clone(),
            _ => Self {
                players: players.clone(),
                tasks: self.tasks.clone(),
                colors: self.colors.clone(),
            },
        }
    }

    fn with_tasks(&self, tasks: &Option<Vec<Task>>) -> Self {
        match tasks {
            None => self.clone(),
            _ => Self {
                players: self.players.clone(),
                tasks: tasks.clone(),
                colors: self.colors.clone(),
            },
        }
    }

    fn with_colors(&self, colors: &Option<Vec<String>>) -> Self {
        match colors {
            None => self.clone(),
            _ => Self {
                players: self.players.clone(),
                tasks: self.tasks.clone(),
                colors: colors.clone(),
            },
        }
    }

    fn with_example(&self, example: &Option<String>) -> Self {
        match example {
            None => self.clone(),
            Some(file) => {
                let xdg_dirs = xdg::BaseDirectories::with_prefix("shortcut")
                    .expect("unable to find xdg base directories");
                let settings =
                    Self::read(&xdg_dirs.find_config_file(format!("examples/{}.toml", file)));
                self.merge(settings)
            }
        }
    }

    fn merge(&self, settings: Self) -> Self {
        self.with_players(&settings.players)
            .with_tasks(&settings.tasks)
            .with_colors(&settings.colors)
    }

    fn as_color(hex_code: &str) -> Result<Color, ParseIntError> {
        // u8::from_str_radix(src: &str, radix: u32) converts a string
        // slice in a given base to u8
        let r: u8 = u8::from_str_radix(&hex_code[1..3], 16)?;
        let g: u8 = u8::from_str_radix(&hex_code[3..5], 16)?;
        let b: u8 = u8::from_str_radix(&hex_code[5..7], 16)?;

        Ok(Color::from_rgb8(r, g, b))
    }
}

pub fn init() {
    let xdg_dirs =
        xdg::BaseDirectories::with_prefix("shortcut").expect("unable to find xdg base directories");
    for file in [
        "config.toml",
        "examples/seven_summits.toml",
        "examples/capitals.toml",
    ] {
        init_config(file, &xdg_dirs);
    }
}

fn init_config(file: &str, xdg_dirs: &xdg::BaseDirectories) {
    if xdg_dirs.find_config_file(file).is_none() {
        let url = format!(
            "https://codeberg.org/daschw/shortcut/raw/branch/main/{}",
            file
        );
        let mut response = reqwest::blocking::get(url).expect("unable to download file");
        let mut target = File::create(
            xdg_dirs
                .place_config_file(file)
                .expect("unable to place config file"),
        )
        .expect("unable to create target file");
        response.copy_to(&mut target).expect("unable to copy data");
    }
}
