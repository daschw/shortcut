use geo::{coord, Coord, EuclideanDistance};
use iced::Color;
use std::cmp::Ordering;

// -----------------------------------------------------------------------------------------
// Player
// -----------------------------------------------------------------------------------------

/// A player with a name, a color, and guesses and scores for all considered tasks.
#[derive(Debug, Clone)]
pub struct Player {
    /// The name of the player.
    pub name: String,
    /// The player's color.
    pub color: Color,
    /// The player's guesses as coordinates in the task's map's target projection.
    guesses: Vec<Coord<f32>>,
    /// Distances from the player's guesses and the task's targets in m.
    distances: Vec<f32>,
    /// Points scored by the player in each game.
    points: Vec<usize>,
}

impl Player {
    /// Create a new Player.
    ///
    /// # Arguments
    ///
    /// * `name` - The player's name.
    /// * `color` - The player's color.
    /// * `n` - Number of tasks to be played.
    pub fn new(name: &str, color: Color, n: usize) -> Self {
        Self {
            name: String::from(name),
            color,
            guesses: vec![coord! {x: 0.0_f32, y: 0.0_f32}; n],
            distances: vec![0.0_f32; n],
            points: vec![0; n],
        }
    }

    /// Set a guess and calculate the euclidian distance to the target.
    ///
    /// # Arguments
    ///
    /// * `task` - Number of the current task.
    /// * `point` - Coordinates of the player's guess.
    /// * `target` - Coordinates of the task's target.
    pub fn guess(&mut self, task: usize, point: &Coord<f32>, target: &Coord<f32>) {
        self.guesses[task] = *point;
        self.distances[task] = point.euclidean_distance(target);
    }

    /// Earn points for a task.
    ///
    /// # Arguments
    ///
    /// * `task` - Number of the current task.
    /// * `points` - Points earned by the player.
    pub fn earn(&mut self, task: usize, points: usize) {
        self.points[task] = points;
    }

    /// Get the points earned for a task.
    ///
    /// # Arguments
    ///
    /// * `task` - Number of the current task.
    pub fn points(&self, task: usize) -> usize {
        self.points[task]
    }

    /// Get a player's total score.
    pub fn score(&self) -> usize {
        self.points.iter().sum()
    }

    /// Get the distance reached for a task.
    ///
    /// # Arguments
    ///
    /// * `task` - Number of the current task.
    pub fn distance(&self, task: usize) -> f32 {
        self.distances[task]
    }

    /// Pretty print the distance reached for a task.
    /// Distances below 1 km are shown in m, others in km.
    ///
    /// Arguments
    ///
    /// * `task` - Number of the current task.
    pub fn pretty_distance(&self, task: usize) -> String {
        let dist = self.distance(task);
        if dist < 1000.0 {
            format!("{} m", dist.round())
        } else {
            format!("{} km", (dist / 1000.0).round())
        }
    }
}

impl Ord for Player {
    /// Players are sorted by their score.
    fn cmp(&self, other: &Self) -> Ordering {
        self.score().cmp(&other.score())
    }
}

impl PartialOrd for Player {
    /// Players are sorted by their score.
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Player {
    /// Players are sorted by their score.
    fn eq(&self, other: &Self) -> bool {
        self.score() == other.score()
    }
}

impl Eq for Player {}
