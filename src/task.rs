use geo::{coord, Coord, LineString, MultiPolygon, Polygon, Transform};
use proj::Proj;
use serde::Deserialize;
use shapefile::dbase::{FieldValue, Record};
use std::fs::File;

// -----------------------------------------------------------------------------------------
// Task
// -----------------------------------------------------------------------------------------

/// A single task in a game.
/// It describes the map to be drawn and the point that has to be found by each player.
#[derive(Debug, Deserialize, Clone)]
pub struct Task {
    /// The question to be displayed at the top of the window.
    /// It should hint to the point that should be found in the map.
    pub question: String,
    /// The coordinates of the point to be found in `(lat, lon)`.
    target: (f32, f32),
    /// The category of maps to be drawn.
    /// Supported values are `"kontinente"`, `"staaten"`, `"bundeslaender"` and `"gemeinden"`.
    category: String,
    /// The map within the selected category.
    /// * `"kontinente"`: `"Africa"`, `"Europe"`, ...
    /// * `"staaten"`: `"AT"`, `"RO"`, ...
    /// * `"bundeslaender"`: `"Oberösterreich"`, `"Wien"`, ...
    /// * `"gemeinden"`: `"Kematen am Innbach"`, `"Krenglbach"`, ...
    instance: String,
}

impl Default for Task {
    /// The default task is to find Moldoveanu, the highest peak in Romania.
    fn default() -> Self {
        Task {
            question: "Where is the highest point on this map".to_string(),
            target: (45.6004, 24.7374),
            category: "staaten".to_string(),
            instance: "RO".to_string(),
        }
    }
}

impl Task {
    /// Get the task's shape.
    /// If the task's instance is not found within its category all shapes of the category are
    /// returned.
    pub fn shape(&self) -> MultiPolygon<f32> {
        let xdg_dirs = xdg::BaseDirectories::with_prefix("shortcut").unwrap();
        let filename = format!("assets/{}.shp", self.category());
        let shapes = shapefile::read_as::<_, shapefile::Polygon, Record>(
            xdg_dirs.find_data_file(filename).unwrap(),
        )
        .expect("Could not open shapefile");
        for (shape, record) in shapes {
            for (name, value) in record {
                if name == self.instance_field()
                    && value == FieldValue::Character(Some(self.instance.clone()))
                {
                    return self.transform(shape.into());
                }
            }
        }
        self.all_shapes()
    }

    /// Get the task's target coordinates in its map's projection.
    pub fn target(&self) -> Coord<f32> {
        let dst = self.dst_projection();
        let (lat, lon) = self.target;
        let t = Proj::new_known_crs("EPSG:4326", dst, None).unwrap();
        let (x, y) = t.convert((lon, lat)).unwrap();
        coord! { x: x, y: y }
    }

    /// Get the task's map category.
    /// This returns either `"gemeinden"`, `"bundeslaender"`, `"staaten"` or `"kontinente"`.
    fn category(&self) -> &str {
        let ret = &self.category as &str;
        match ret {
            "gemeinden" | "bundeslaender" | "staaten" => ret,
            _ => "kontinente",
        }
    }

    /// Get the name of the field that describes a task's instance in the corresponding shapefile.
    fn instance_field(&self) -> &str {
        match &self.category as &str {
            "gemeinden" => "GEM_NAME",
            "staaten" => "CNTR_CODE",
            "bundeslaender" => "NUTS_NAME",
            _ => "CONTINENT",
        }
    }

    /// Transform a multipoligon from the task's source projection to its target projection.
    /// Convert all coordinates to `f32` and return the polygon in the target projection.
    ///
    /// Arguments
    ///
    /// * `m64` - A `MultiPolygon<f64>` read from the shapefile corresponding to the task's
    ///   category
    fn transform(&self, m64: MultiPolygon<f64>) -> MultiPolygon<f32> {
        let mut mpoly = m64;
        let src = self.src_projection();
        let dst = self.dst_projection();
        if src != dst {
            let t = Proj::new_known_crs(src, dst, None).unwrap();
            mpoly.transform(&t).unwrap();
        }
        return MultiPolygon::<f32>::new(
            mpoly
                .iter()
                .map(|poly| {
                    Polygon::<f32>::new(
                        as_f32(poly.exterior()),
                        poly.interiors().iter().map(as_f32).collect(),
                    )
                })
                .collect(),
        );
    }

    /// Get the source projection of a task.
    /// This is the projection that the corresponding shapefile is provided with.
    fn src_projection(&self) -> &str {
        match &self.category as &str {
            "gemeinden" => "EPSG:31255",
            "kontinente" => "EPSG:4326",
            _ => "EPSG:3035",
        }
    }

    /// Get the target projection of a task.
    /// This is the projection that the map will be drawn in on the canvas.
    fn dst_projection(&self) -> &str {
        match &self.category as &str {
            "gemeinden" => "EPSG:31255",
            "kontinente" => "EPSG:3857",
            _ => "EPSG:3035",
        }
    }

    /// Get all shapes of a task's category.
    /// This is only intended as fallback if the task's instance is not found.
    fn all_shapes(&self) -> MultiPolygon<f32> {
        let xdg_dirs = xdg::BaseDirectories::with_prefix("shortcut").unwrap();
        let filename = format!("assets/{}.shp", self.category());
        let shapes = shapefile::read_as::<_, shapefile::Polygon, Record>(
            xdg_dirs.find_data_file(filename).unwrap(),
        )
        .unwrap();
        let mut polys: Vec<Polygon<f64>> = vec![];
        for (shape, _record) in shapes {
            let mpoly: MultiPolygon<f64> = shape.into();
            for poly in mpoly.iter() {
                polys.push(poly.clone())
            }
        }
        self.transform(MultiPolygon::new(polys))
    }
}

// -----------------------------------------------------------------------------------------
// Init
// -----------------------------------------------------------------------------------------

/// Initialize the data required for tasks.
/// Check if all shapefiles are available in `$XDG_DATA_DIR/shortcut/assets/`.
/// Otherwise download the missing files from my nextcloud instance.
pub fn init() {
    let xdg_dirs = xdg::BaseDirectories::with_prefix("shortcut").unwrap();
    for category in ["gemeinden", "bundeslaender", "staaten", "kontinente"] {
        init_category(category, &xdg_dirs)
    }
}

/// Download the `.dbf` and `.shp` files for a category and save them to
/// `$XDG_DATA_DIR/shortcut/assets/` if they are not already present.
///
/// # Arguments
///
/// * `category` - Either `"gemeinden"`, `"bundeslaender"`, `"staaten"` or `"kontinente"`.
/// * `xdg_dirs` - XDG directories for shortcut.
fn init_category(category: &str, xdg_dirs: &xdg::BaseDirectories) {
    for ext in ["dbf", "shp"] {
        let filename = format!("{}.{}", category, ext);
        let dst = format!("assets/{}", filename);
        if xdg_dirs.find_data_file(&dst).is_none() {
            let url = format!(
                "{}{}{}",
                "https://littleearthling.owncube.com/index.php/s/2imMQ2MPyBfsCBa/download",
                "?path=/&files=",
                filename
            );
            let mut response = reqwest::blocking::get(url).expect("unable to download file");
            let mut target = File::create(
                xdg_dirs
                    .place_data_file(dst)
                    .expect("unable to place data file"),
            )
            .expect("unable to create target file");
            response.copy_to(&mut target).expect("unable to copy data");
        }
    }
}

// -----------------------------------------------------------------------------------------
// Utils
// -----------------------------------------------------------------------------------------

/// Convert a linestring's coordinates from `f64` to `f32`.
///
/// Arguments
///
/// * `ls64`: Original `LineString<f64>`.
fn as_f32(ls64: &LineString<f64>) -> LineString<f32> {
    LineString::<f32>::new(
        ls64.coords()
            .map(|coord| {
                coord! {
                    x: coord.x as f32,
                    y: coord.y as f32,
                }
            })
            .collect(),
    )
}
