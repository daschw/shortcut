use crate::{player::Player, shortcut::Message, task::Task};

use std::fmt::Debug;
use std::num::ParseIntError;

use geo::{coord, BoundingRect, Coord, LineString, MultiPolygon};
use iced::{
    mouse,
    widget::canvas::{
        self,
        event::{self, Event},
        path::{
            self,
            lyon_path::geom::euclid::{Point2D, Transform2D, UnknownUnit, Vector2D},
        },
        Cache, Fill, Path, Stroke,
    },
    Color, Point, Rectangle, Renderer, Size, Theme,
};

// -----------------------------------------------------------------------------------------
// Map state
// -----------------------------------------------------------------------------------------

#[derive(Debug)]
/// State of the map.
pub enum MapState {
    /// All guesses and the target position are revealed.
    Reveal,
    /// One of the players is guessing.
    Guessing(usize),
}

impl Default for MapState {
    /// A map starts with the first player guessing.
    fn default() -> Self {
        Self::Guessing(0)
    }
}

// -----------------------------------------------------------------------------------------
// Map
// -----------------------------------------------------------------------------------------

/// A Map holds information on the current shape polygon, the target coordinates, the
/// guesses and colors for each player and its state.
#[derive(Debug)]
pub struct Map {
    /// The shape polygon describing the boundaries.
    pub shape: Option<MultiPolygon<f32>>,
    /// The target coordinates.
    pub target: Option<Coord<f32>>,
    /// The guesses taken by each player.
    pub guesses: Vec<Option<Coord<f32>>>,
    /// The colors corresponding to each player.
    colors: Vec<Color>,
    /// The current state.
    state: MapState,
    /// A cache for the map shape.
    pub shape_cache: Cache,
    /// A cache for the player guesses.
    pub guess_cache: Cache,
}

impl Map {
    /// Create a new map.
    ///
    /// # Arguments
    ///
    /// * `players` - `Vec` of `Player`s competing on this map.
    pub fn new(players: &Vec<Player>) -> Self {
        Self {
            shape: None,
            target: None,
            guesses: vec![None; players.len()],
            colors: players.iter().map(|p| p.color).collect(),
            state: MapState::Guessing(0),
            shape_cache: Cache::default(),
            guess_cache: Cache::default(),
        }
    }

    /// Switch to the next task and reset players and guesses.
    ///
    /// # Arguments
    ///
    /// * `task` - The new `Task` for the map.
    /// * `players` - Players competing in the new task.
    pub fn next_task(&mut self, task: &Task, players: &[Player]) {
        self.shape = Some(task.shape());
        self.target = Some(task.target());
        self.guesses = vec![None; self.guesses.len()];
        self.colors = players.iter().map(|p| p.color).collect();
        self.state = MapState::Guessing(0);
        self.shape_cache.clear();
        self.guess_cache.clear();
    }

    /// Set a player's guess.
    ///
    /// # Arguments
    ///
    /// * `player` - Number of the current player.
    /// * `x` - x coordinate of the player's guess.
    /// * `y` - y coordinate of the player's guess.
    pub fn guess(&mut self, player: usize, x: f32, y: f32) {
        self.guesses[player] = Some(coord! { x: x, y: y });
        self.guess_cache.clear();
    }

    /// Reveal the guesses of all players.
    pub fn reveal(&mut self) {
        self.state = MapState::Reveal;
        self.guess_cache.clear();
    }

    /// Switch to the next player.
    ///
    /// # Arguments
    ///
    /// * `player` - Number of the next player.
    pub fn next_player(&mut self, player: usize) {
        self.state = MapState::Guessing(player);
        self.guess_cache.clear();
    }

    /// Get the transform from the map's shape's coordinate system to the canvas' screen size.
    ///
    /// # Arguments
    ///
    /// * `screen` - Size of the screen canvas.
    fn transform(&self, screen: &Size) -> Transform2D<f32, UnknownUnit, UnknownUnit> {
        match &self.shape {
            Some(mpoly) => {
                let world = mpoly.bounding_rect().unwrap();
                let t = Transform2D::identity()
                    .then_translate(Vector2D::from([-world.min().x, -world.max().y]));
                let mut offset = [0.0, 0.0];
                let scale;
                if screen.height / world.height() < screen.width / world.width() {
                    scale = screen.height / world.height();
                    offset[0] = (screen.width - scale * world.width()) * 0.5;
                } else {
                    scale = screen.width / world.width();
                    offset[1] = (screen.height - scale * world.height()) * 0.5;
                }
                t.then_scale(0.9 * scale, -0.9 * scale)
                    .then_translate(Vector2D::from([
                        0.05 * screen.width + offset[0],
                        0.05 * screen.height + offset[1],
                    ]))
            }
            None => Transform2D::identity(),
        }
    }
}

// -----------------------------------------------------------------------------------------
// Program
// -----------------------------------------------------------------------------------------

impl canvas::Program<Message> for Map {
    type State = ();

    // -------------------------------------------------------------------------------------
    // View logic
    // -------------------------------------------------------------------------------------

    fn draw(
        &self,
        _state: &Self::State,
        renderer: &Renderer,
        _theme: &Theme,
        bounds: Rectangle,
        _cursor: mouse::Cursor,
    ) -> Vec<canvas::Geometry> {
        let border = self
            .shape_cache
            .draw(renderer, bounds.size(), |frame| match &self.shape {
                Some(mpoly) => {
                    let to_screen = self.transform(&bounds.size());
                    for poly in mpoly.iter() {
                        frame.fill(
                            &transform_path(poly.exterior(), &to_screen),
                            as_color("#2E2C2F").unwrap(),
                        );
                        for interior in poly.interiors() {
                            frame.fill(
                                &transform_path(interior, &to_screen),
                                as_color("#FFFFFF").unwrap(),
                            );
                        }
                    }
                }
                None => {}
            });

        let points = self.guess_cache.draw(renderer, bounds.size(), |frame| {
            let to_screen = self.transform(&bounds.size());
            match self.state {
                MapState::Reveal => {
                    let t = transform_point(&self.target.unwrap(), &to_screen);
                    for (i, coord) in self.guesses.iter().enumerate() {
                        let p = transform_point(&coord.unwrap(), &to_screen);
                        frame.stroke(
                            &Path::line(p, t),
                            Stroke::default().with_color(self.colors[i]),
                        );
                        frame.fill(&Path::circle(p, 5.0), Fill::from(self.colors[i]));
                    }
                    frame.fill(&Path::circle(t, 5.0), as_color("#E1E4DC").unwrap());
                }
                MapState::Guessing(player) => {
                    if let Some(guess) = self.guesses[player] {
                        let g = transform_point(&guess, &to_screen);
                        frame.fill(&Path::circle(g, 5.0), Fill::from(self.colors[player]));
                    }
                }
            }
        });

        vec![border, points]
    }

    // -------------------------------------------------------------------------------------
    // Update logic
    // -------------------------------------------------------------------------------------

    fn update(
        &self,
        _state: &mut Self::State,
        event: Event,
        bounds: Rectangle,
        cursor: mouse::Cursor,
    ) -> (event::Status, Option<Message>) {
        match self.state {
            MapState::Reveal => (event::Status::Ignored, None),
            MapState::Guessing(_) => {
                let cursor_position = if let Some(position) = cursor.position_in(bounds) {
                    position
                } else {
                    return (event::Status::Ignored, None);
                };

                match event {
                    Event::Mouse(mouse_event) => {
                        let message = match mouse_event {
                            iced::mouse::Event::ButtonPressed(iced::mouse::Button::Left) => {
                                let to_world = self.transform(&bounds.size()).inverse().unwrap();
                                let (x, y) = transform_guess(&cursor_position, &to_world);
                                Some(Message::PointAdded(x, y))
                            }
                            _ => None,
                        };
                        (event::Status::Captured, message)
                    }
                    _ => (event::Status::Ignored, None),
                }
            }
        }
    }
}

// -----------------------------------------------------------------------------------------
// Utils
// -----------------------------------------------------------------------------------------

/// Transform a path.
///
/// # Arguments
///
/// * `ls` - A linestring describing the path.
/// * `t` - The transform applied to the path's coordinates.
fn transform_path(ls: &LineString<f32>, t: &Transform2D<f32, UnknownUnit, UnknownUnit>) -> Path {
    let mut coords = ls.coords();
    let mut p = path::Builder::new();
    p.move_to(point(coords.next().unwrap()));
    for c in coords {
        p.line_to(point(c));
    }
    p.close();
    p.build().transform(t)
}

/// Transform a point.
///
/// # Arguments
///
/// * `c` - The point's coordinates.
/// * `t` - THe transform applied to the point.
fn transform_point(c: &Coord<f32>, t: &Transform2D<f32, UnknownUnit, UnknownUnit>) -> Point {
    let p = t.transform_point(Point2D::new(c.x, c.y));
    Point::new(p.x, p.y)
}

/// Transform a guess.
///
/// # Arguments
///
/// * `p` - The point to transform.
/// * `t` - The transform applied to the point.
fn transform_guess(p: &Point, t: &Transform2D<f32, UnknownUnit, UnknownUnit>) -> (f32, f32) {
    let guess = t.transform_point(Point2D::new(p.x, p.y));
    (guess.x, guess.y)
}

/// Convert a `Coord` to a `Point`.
///
/// # Arguments
///
/// * `c` - Coordinates.
fn point(c: &Coord<f32>) -> Point {
    Point::new(c.x, c.y)
}

/// Convert a hex_code string to a color.
///
/// # Arguments
///
/// - `hex_code` - A string of the form "#......".
fn as_color(hex_code: &str) -> Result<Color, ParseIntError> {
    // u8::from_str_radix(src: &str, radix: u32) converts a string
    // slice in a given base to u8
    let r: u8 = u8::from_str_radix(&hex_code[1..3], 16)?;
    let g: u8 = u8::from_str_radix(&hex_code[3..5], 16)?;
    let b: u8 = u8::from_str_radix(&hex_code[5..7], 16)?;

    Ok(Color::from_rgb8(r, g, b))
}
