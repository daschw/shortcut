use crate::config::Settings;
use crate::map::Map;
use crate::player::Player;
use crate::task::Task;
use iced::executor;
use iced::widget::{button, canvas, column, row, text};
use iced::{Application, Command, Length, Theme};

// -----------------------------------------------------------------------------------------
// Shortcut
// -----------------------------------------------------------------------------------------

/// The main Application struct.
/// It tracks all tasks, all players, the current map and the current stage in the game.
pub struct Shortcut {
    tasks: Vec<Task>,
    players: Vec<Player>,
    map: Map,
    stage: Stage,
}

/// All possible shortcut game stages.
pub enum Stage {
    Start,
    Guessing(usize, usize),
    Reveal(usize),
    Scores,
}

// -----------------------------------------------------------------------------------------
// Messages
// -----------------------------------------------------------------------------------------

/// All shortcut game messages.
#[derive(Debug, Clone, Copy)]
pub enum Message {
    PointAdded(f32, f32),
    NextPressed,
}

// -----------------------------------------------------------------------------------------
// Application
// -----------------------------------------------------------------------------------------

impl Application for Shortcut {
    type Executor = executor::Default;
    type Message = Message;
    type Theme = Theme;
    type Flags = Settings;

    /// Create a new shortcut application.
    ///
    /// # Arguments
    ///
    /// * `settings` - The settings parsed from command line input.
    fn new(settings: Self::Flags) -> (Self, Command<Self::Message>) {
        let players = settings.players();
        (
            Self {
                tasks: settings.tasks(),
                players: players.clone(),
                map: Map::new(&players),
                stage: Stage::Start,
            },
            Command::none(),
        )
    }

    /// The title for the application window is "Shortcut".
    fn title(&self) -> String {
        String::from("Shortcut")
    }

    // -------------------------------------------------------------------------------------
    // View logic
    // -------------------------------------------------------------------------------------

    /// The UI consists of
    ///
    /// * a top frame holding the current task's question,
    /// * a main frame showing the current task's map with the players' guesses,
    /// * a bottom frame with instructions or information about the game stage, and
    /// * a single button to navigate through the game.
    fn view(&self) -> iced::Element<'_, Self::Message> {
        // The top row holds a text message.
        let mut ui = column![row![text(match self.stage {
            // If we start the game we show a welcome message.
            Stage::Start => "Welcome to shortcut!",
            // If the game is not yet finished we show the question of the current task.
            Stage::Guessing(task, _) | Stage::Reveal(task) => &self.tasks[task].question,
            // Otherwise we present the winner.
            Stage::Scores => "And the winner is ...",
        })]
        .padding(10)
        .spacing(20)];
        // The main frame either shows a map or a table with the final player scores.
        match self.stage {
            // If we show the scores we have to build the table with all players' scores
            // for each task and the final score.
            Stage::Scores => {
                let mut table = row![];
                // add a column with each player's name.
                let mut names = column![text("Round")];
                for p in &self.players {
                    names = names.push(text(&p.name).style(p.color));
                }
                table = table.push(names);
                // add columns with the scores of each player for each task.
                for i in 0..self.tasks.len() {
                    let mut points = column![text(format!("{}", i + 1))];
                    for p in &self.players {
                        points = points.push(text(p.points(i)).style(p.color));
                    }
                    table = table.push(points);
                }
                // add a column with the final scores for each player.
                let mut scores = column![text("Total")];
                for p in &self.players {
                    scores = scores.push(text(p.score()).style(p.color));
                }
                table = table
                    .push(scores)
                    .padding(10)
                    .spacing(20)
                    .align_items(iced::Alignment::Center);
                ui = ui.push(
                    column![table]
                        .align_items(iced::Alignment::Center)
                        .width(Length::Fill)
                        .height(Length::Fill),
                )
            }
            // Otherwise we show the current map.
            _ => {
                ui = ui.push(canvas(&self.map).width(Length::Fill).height(Length::Fill));
            }
        }
        // the bottom frame shows some information depending on the game's stage.
        ui = ui.push(
            match self.stage {
                // If we start we show how to continue.
                Stage::Start => row![text("Click Next to continue.")],
                // If a player is guessing we show this information and print the name of
                // the player in their color.
                Stage::Guessing(_, player) => {
                    row![text(format!("{} is guessing", &self.players[player].name))
                        .style(self.players[player].color)]
                }
                // If all guesses are revealed we show the distances of each player's
                // guesses to the current task's target in the players' colors.
                Stage::Reveal(task) => {
                    let mut results = row![];
                    for p in &self.players {
                        results = results.push(
                            text(format!("{}: {}", p.name, p.pretty_distance(task))).style(p.color),
                        );
                    }
                    results
                }
                // If the scores are shown we show the winner in their color.
                Stage::Scores => {
                    row![text(&self.players[0].name).style(self.players[0].color)]
                }
            }
            .padding(10)
            .spacing(20),
        );
        // The text shown on the bottom button depends on the current stage of the game.
        ui = ui.push(
            button(text(match self.stage {
                Stage::Start => "Start",
                Stage::Guessing(_, _) => "Confirm",
                Stage::Reveal(_) => "Continue",
                Stage::Scores => "Quit",
            }))
            .on_press_maybe(match self.stage {
                Stage::Guessing(_, player) => {
                    self.map.guesses[player].map(|_| Message::NextPressed)
                }
                _ => Some(Message::NextPressed),
            }),
        );
        ui.width(Length::Fill)
            .align_items(iced::Alignment::Center)
            .into()
    }

    // -------------------------------------------------------------------------------------
    // Update logic
    // -------------------------------------------------------------------------------------

    /// There are two possible user actions in this game:
    ///
    /// * The `Start`/`Confirm`/`Next`/`Quit` button is pressed.
    /// * A point on the map is selected.
    fn update(&mut self, message: Message) -> Command<Message> {
        match message {
            // If a point is added we update the player's guess.
            Message::PointAdded(x, y) => {
                if let Stage::Guessing(_, player) = self.stage {
                    self.map.guess(player, x, y);
                }
            }
            // The action invoked by clicking the button depends on the stage.
            Message::NextPressed => match self.stage {
                // If the game is just starting we show the first task and let the first
                // player guess.
                Stage::Start => {
                    self.map.next_task(&self.tasks[0], &self.players);
                    self.stage = Stage::Guessing(0, 0);
                }
                // If a player just guessed we save the last guess.
                Stage::Guessing(task, player) => {
                    self.players[player].guess(
                        task,
                        &self.map.guesses[player].unwrap(),
                        &self.map.target.unwrap(),
                    );
                    // If the last player guessed we order the players by the distances of
                    // their guesses to the target and assign points to each player.
                    // Finally, all guesses and the target are revealed on the map.
                    // Otherwise it's the next player's turn to guess.
                    if player == self.players.len() - 1 {
                        let distances: Vec<isize> = self
                            .players
                            .iter()
                            .map(|p| p.distance(task).round() as isize)
                            .collect();
                        let permutation = permutation::sort(distances);
                        let points: Vec<usize> = (0..self.players.len()).rev().collect();
                        let ordered_points = permutation.apply_slice(points);
                        for (i, p) in ordered_points.iter().enumerate() {
                            self.players[i].earn(task, *p)
                        }
                        self.map.reveal();
                        self.stage = Stage::Reveal(task);
                    } else {
                        self.map.next_player(player + 1);
                        self.stage = Stage::Guessing(task, player + 1);
                    }
                }
                // If all guesses are revealed the players are sorted by their score.
                Stage::Reveal(task) => {
                    self.players.sort();
                    self.players.reverse();
                    // If we are at the last task we show the scores.
                    // Otherwise we continue to the next task.
                    if task == self.tasks.len() - 1 {
                        self.stage = Stage::Scores;
                    } else {
                        self.map.next_task(&self.tasks[task + 1], &self.players);
                        self.stage = Stage::Guessing(task + 1, 0);
                    }
                }
                // If we have seen the scores we can close the window.
                Stage::Scores => {
                    return iced::window::close();
                }
            },
        }
        Command::none()
    }
}
