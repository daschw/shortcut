use clap::Parser;
use config::{Args, Settings};
use iced::Application;
use shortcut::Shortcut;

mod config;
mod map;
mod player;
mod shortcut;
mod task;

fn main() -> iced::Result {
    task::init();
    config::init();
    let args = Args::parse();
    let settings = Settings::new(&args);
    Shortcut::run(iced::Settings::with_flags(settings))
}
